const express = require("express");
const path = require("path");
const indexRoute = require("./routes/index");
const bodyParser = require("body-parser");


// Initiate the express App
const app = express();


// SET View Engine
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");


// Set Static files
app.use(express.static(path.join(__dirname, "public")));


// Takes the raw requests and turns them into usable properties on req.body
app.use(bodyParser.json());
app.use(
    bodyParser.urlencoded({
        extended: true
    })
);


// Route Handling App level Middleware
app.use("/", indexRoute);


//The 404 Route
app.get("*", function (req, res) {
    res.render("404");
});

module.exports = app;