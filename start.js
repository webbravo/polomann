// import environmental variables from our variables.env file
require("dotenv").config({
  path: "var.env"
});

// READY?! Let's go!

// Start our app!
const app = require("./app");

app.set("port", process.env.PORT);
const server = app.listen(app.get("port"), () => {
  console.debug(`Express running → PORT ${server.address().port}`);
});