const express = require("express");

let router = express.Router();

router.get("/", (req, res) => {
  res.render("index-landing-page");
});

module.exports = router;
module.exports = router;